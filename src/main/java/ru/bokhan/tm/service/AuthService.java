package ru.bokhan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.service.IAuthService;
import ru.bokhan.tm.api.service.IUserService;
import ru.bokhan.tm.entity.User;
import ru.bokhan.tm.enumerated.Role;
import ru.bokhan.tm.exception.empty.EmptyLoginException;
import ru.bokhan.tm.exception.empty.EmptyPasswordException;
import ru.bokhan.tm.exception.security.AccessDeniedException;
import ru.bokhan.tm.util.HashUtil;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @NotNull
    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public void checkRoles(@Nullable final Role[] roles) {
        if (roles == null || roles.length == 0) return;
        @NotNull final String userId = getUserId();
        @Nullable final User user = userService.findById(userId);
        if (user == null) return;
        @Nullable final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (@Nullable final Role item : roles) if (role.equals(item)) return;
        throw new AccessDeniedException();
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }


    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.getLocked()) throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        userService.create(login, password, email);
    }

}
