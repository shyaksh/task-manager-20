package ru.bokhan.tm.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.service.IDomainService;
import ru.bokhan.tm.api.service.IProjectService;
import ru.bokhan.tm.api.service.ITaskService;
import ru.bokhan.tm.api.service.IUserService;
import ru.bokhan.tm.dto.Domain;

@AllArgsConstructor
public final class DomainService implements IDomainService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    @Override
    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
        userService.load(domain.getUsers());
        projectService.load(domain.getProjects());
        taskService.load(domain.getTasks());
    }

    @Override
    public void export(@Nullable final Domain domain) {
        if (domain == null) return;
        domain.setProjects(projectService.findAll());
        domain.setUsers(userService.findAll());
        domain.setTasks(taskService.findAll());
    }

}
