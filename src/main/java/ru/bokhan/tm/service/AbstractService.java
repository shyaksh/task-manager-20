package ru.bokhan.tm.service;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.repository.IRepository;
import ru.bokhan.tm.api.service.IService;
import ru.bokhan.tm.entity.AbstractEntity;
import ru.bokhan.tm.exception.incorrect.IncorrectDataFileException;

import java.util.List;

@AllArgsConstructor
public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IRepository<E> repository;

    @Override
    public @NotNull List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public void load(@Nullable final List<E> list) {
        if (list == null) throw new IncorrectDataFileException();
        repository.load(list);
    }

    @Nullable
    @Override
    public E remove(@Nullable final E e) {
        if (e == null) return null;
        return repository.remove(e);
    }

}
