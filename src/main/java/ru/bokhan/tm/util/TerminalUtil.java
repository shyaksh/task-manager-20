package ru.bokhan.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.bokhan.tm.exception.NotNumberException;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextNumber() {
        @NotNull final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new NotNumberException(value);
        }
    }

}