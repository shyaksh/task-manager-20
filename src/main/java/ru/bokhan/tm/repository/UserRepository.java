package ru.bokhan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.repository.IUserRepository;
import ru.bokhan.tm.entity.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findById(@NotNull final String id) {
        for (@Nullable final User user : entities) {
            if (user == null) continue;
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        for (@Nullable final User user : entities) {
            if (user == null) continue;
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Nullable
    @Override
    public User removeById(@NotNull final String id) {
        @Nullable final User user = findById(id);
        if (user == null) return null;
        return remove(user);
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        @Nullable final User user = findByLogin(login);
        if (user == null) return null;
        return remove(user);
    }

}