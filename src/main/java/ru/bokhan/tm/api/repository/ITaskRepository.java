package ru.bokhan.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    void add(@NotNull String userId, @NotNull Task task);

    void remove(@NotNull String userId, @NotNull Task task);

    void clear(@NotNull String userId);

    @NotNull
    List<Task> findAll(@NotNull String userId);

    @Nullable
    Task findById(@NotNull String userId, @NotNull String id);

    @Nullable
    Task findByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Task findByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task removeById(@NotNull String userId, @NotNull String id);

    @Nullable
    Task removeByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Task removeByName(@NotNull String userId, @NotNull String name);

}