package ru.bokhan.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.entity.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    @NotNull
    List<E> findAll();

    void add(@NotNull E e);

    void addAll(@NotNull List<E> list);

    @Nullable
    E remove(@NotNull E e);

    void load(@NotNull List<E> list);

    void clear();

}
