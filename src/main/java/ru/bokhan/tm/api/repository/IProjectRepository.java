package ru.bokhan.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    void add(@NotNull String userId, @NotNull Project project);

    void remove(@NotNull String userId, @NotNull Project project);

    void clear(@NotNull String userId);

    @NotNull
    List<Project> findAll(@NotNull String userId);

    @Nullable
    Project findById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project findByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project findByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project removeById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project removeByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project removeByName(@NotNull String userId, @NotNull String name);

}