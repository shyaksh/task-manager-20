package ru.bokhan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    void add(@Nullable String userId, @Nullable Task task);

    void remove(@Nullable String userId, @Nullable Task task);

    void clear(@Nullable String userId);

    @NotNull
    List<Task> findAll(@Nullable String userId);

    @Nullable
    Task findById(@Nullable String userId, @Nullable String id);

    @Nullable
    Task findByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    Task findByName(@Nullable String userId, @Nullable String name);

    @Nullable
    Task removeById(@Nullable String userId, @Nullable String id);

    @Nullable
    Task removeByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    Task removeByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    Task updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

}