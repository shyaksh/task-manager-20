package ru.bokhan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.entity.User;
import ru.bokhan.tm.enumerated.Role;

public interface IUserService extends IService<User> {

    @Nullable
    User create(@Nullable String login, @Nullable String password);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    User findById(@Nullable String id);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User removeById(@Nullable String id);

    @Nullable
    User removeByLogin(@Nullable String login);

    @NotNull
    User updateById(
            @Nullable String id, String login,
            @Nullable String firstName, @Nullable String lastName, @Nullable String middleName,
            @Nullable String email
    );

    @NotNull
    User updatePasswordById(@Nullable String id, @Nullable String password);

    @Nullable
    User lockUserByLogin(@Nullable String login);

    @Nullable
    User unlockUserByLogin(@Nullable String login);

    @Nullable
    User removeUserByLogin(@Nullable String login);

}
