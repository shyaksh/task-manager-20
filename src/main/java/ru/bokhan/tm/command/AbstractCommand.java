package ru.bokhan.tm.command;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.service.IServiceLocator;
import ru.bokhan.tm.enumerated.Role;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    @Nullable
    public Role[] roles() {
        return null;
    }

    public abstract String name();

    public abstract String argument();

    public abstract String description();

    public abstract void execute() throws Exception;

}
