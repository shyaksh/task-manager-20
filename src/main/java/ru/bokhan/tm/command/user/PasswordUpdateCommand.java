package ru.bokhan.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.entity.User;
import ru.bokhan.tm.util.TerminalUtil;

public final class PasswordUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "password-update";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update my password";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @Nullable final User user = serviceLocator.getUserService().updatePasswordById(userId, password);
        System.out.println("[OK]");
    }

}
