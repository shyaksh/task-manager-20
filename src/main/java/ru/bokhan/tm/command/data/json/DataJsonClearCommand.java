package ru.bokhan.tm.command.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.constant.DataConstant;
import ru.bokhan.tm.enumerated.Role;

import java.io.File;
import java.nio.file.Files;

public final class DataJsonClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-json-clear";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove json data file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON CLEAR]");
        @NotNull final File file = new File(DataConstant.FILE_JSON);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
