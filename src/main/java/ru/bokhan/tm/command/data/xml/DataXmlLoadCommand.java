package ru.bokhan.tm.command.data.xml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.service.IDomainService;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.constant.DataConstant;
import ru.bokhan.tm.dto.Domain;
import ru.bokhan.tm.enumerated.Role;

import java.io.FileInputStream;

public final class DataXmlLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-xml-load";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from xml file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML LOAD]");
        @NotNull final FileInputStream fileInputStream = new FileInputStream(DataConstant.FILE_XML);
        @NotNull final ObjectMapper mapper = new XmlMapper();
        @NotNull final Domain domain = mapper.readValue(fileInputStream, Domain.class);
        fileInputStream.close();
        @NotNull final IDomainService domainService = serviceLocator.getDomainService();
        domainService.load(domain);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
