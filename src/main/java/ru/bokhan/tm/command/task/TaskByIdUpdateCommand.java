package ru.bokhan.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.entity.Task;
import ru.bokhan.tm.util.TerminalUtil;

public final class TaskByIdUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-update-by-id";
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update task by id";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE TASK]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final Task task = serviceLocator.getTaskService().findById(userId, id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final Task taskUpdated = serviceLocator.getTaskService().updateById(userId, id, name, description);
        System.out.println("[OK]");
    }

}
